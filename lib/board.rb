class Board
  attr_accessor :board
  attr_reader :cols, :rows

  def initialize(cols = 7, rows = 6)
    @cols, @rows = cols, rows
    @board = create_new_board

    ## AI search patterns; more/better still needed!
    @patts = [[:B,:B,:B,nil],
              [:W,:W,:W,nil]]
  end

  def create_new_board
    board = []
    @cols.times do
      board << Array.new(@rows)
    end

    board    
  end

	def render
	  output = "Current board:\n---------------------\n"
    (@rows - 1).downto(0) do |row|
      board.each { |val| output += val[row].nil? ? "\[ \]" : "\[#{val[row].to_s}\]" }
      output += "\n"
    end
	  output += " 0  1  2  3  4  5  6 \n"
	  output += "---------------------\n"
	  
	  output
	end
	
	def place_token(column, color)
		raise "Invalid move, pick a column between 0 and #{@cols - 1}" if column > @cols
    raise "Invalid move, column #{column} is full" if column_full?(column)
		first_open = @board[column].index(nil) 
    @board[column][first_open] = color
		
		print render
	end

  def column_full?(col)
    return true if !@board[col][@rows-1].nil?
    false
  end

  def winner?
  	return horiz_win? || vert_win? || up_diag_win? || down_diag_win?
  end

	def horiz_win?(board = @board)
    copy = @board.transpose
    copy.each do |row|
      (0..3).each do |i|
        winner = row[i..(i+3)].all? { |e| e == row[i] && !e.nil? }
        return true if winner
      end     
    end

    false   
	end

	def vert_win?
    board.each do |col|
      (0..2).each do |i|
        winner = col[i..(i+3)].all? { |e| e == col[i] && !e.nil? }
        return true if winner
      end
    end

    false
	end
 
  def up_diag_win?
    (0..3).each do |col_idx|
      # (0..3).each do |col_rows|                 # bug fix: should be (0..2)
      (0..2).each do |col_rows|
        diags = get_up_diags(col_idx, col_rows)
        if diags.all?{ |el| diags.first == el && !el.nil?}
          return true
        end
      end
    end

    false
  end

  def down_diag_win?
    (0..3).each do |col_idx|
      # (3..6).each do |col_rows|                 # bug fix should be (3..5)
      (3..5).each do |col_rows|
        diags = get_down_diags(col_idx, col_rows)
        if diags.all?{ |el| diags.first == el && !el.nil?}
          return true
        end
      end
    end

    false
  end

  def get_up_diags(col_idx, col_rows)
    diags = []
    (col_idx..(col_idx + 3)).each_with_index do |col, rows|
      diags << board[col][col_rows + rows]
    end

    diags
  end

  def get_down_diags(col_idx, col_rows)
    diags = []
    (col_idx..(col_idx + 3)).each_with_index do |col, rows|
      diags << board[col][col_rows - rows]
    end

    diags
  end

  #### AI functions:
  # return col # of best move if found or false
  # - Easiest case is verticals: look for 3 tokens of same (either) color 
  #   followed by open slot (nil)
  # - for horizontal and diagonals, patts will have to account for nil
  #    on either end OR in middle, AND check that suggested slot is 
  #    playable (token in place in row below it) 
  # CpuPlayer is currently always Black (:B), so search for :B  patt first 
  # (play to win!), then search for :W patt to play blocking move
  # NOTE: this strategy is flawed until all vert/horz/diag ":B" patterns
  # are searched before ":W" patterns

  def move_suggest
    return vert_suggest || horz_suggest || up_diag_suggest || down_diag_suggest
  end
  
  def vert_suggest
    result = false

    @patts.each do |patt|
      @board.each_with_index do |col,ci|
        (0..2).each do |e|
          if col[e..e+3] == patt
            result = ci
            return result
          end
        end
      end
    end
    result
  end  

  def horz_suggest
    result = false
    copy = @board.transpose

    @patts.each do |patt|
      copy.each_with_index do |row,ri|
        (0..3).each_with_index do |e, ei|
          if row[e..e+3] == patt 
            if ri == 0 || @board[ei][ri-1] != nil
              result = ei+3
              return result
            end
          elsif row[e..e+3] == patt.reverse 
            if ri == 0 || @board[ei][ri-1] != nil
              result = ei
              return result
            end          
          end
        end
      end
    end

    result
  end

  def up_diag_suggest
    return false
  end

  def down_diag_suggest
    return false
  end

end