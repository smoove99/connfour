class Player
	attr_reader :color

	def initialize(color)
		@color = color
	end

  def do_turn(board)
    column_range = board.cols
    begin
  		board.place_token(get_move(board, column_range), @color)
    rescue Exception => e
      puts "Error: #{e}"
      retry
    end

  end
  # private

end

class PersonPlayer < Player
  
  private
  
  def get_move(board, col_range)
    while(true)
      puts "Pick a column (0 to #{col_range - 1}) "
      input = gets.chomp
      if input !~ /[0-#{col_range}]/
      	puts "Invalid input! Try again" 
      else
      	answer = input.to_i
      	break
      end
    end
    answer
  end  

end

class CpuPlayer < Player
  
  def find_best_move(board)
    board.move_suggest
  end

  private
  
  def get_move(board,col_range)
    return find_best_move(board) || rand(col_range)
  end

end

