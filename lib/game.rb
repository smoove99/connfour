require_relative 'player'
require_relative 'board'

class Game
	attr_reader :players, :board

  def initialize(
  	player_one = CpuPlayer.new(:W), 
		player_two = CpuPlayer.new(:B)
		)
		
		@board = Board.new()
		@players = {
			white: player_one,
			black: player_two
		}

  end

  def play_game
  	print @board.render
  	turns = 0
  	max = @board.cols * @board.rows

		until turns >= max || @board.winner?		
			puts "#{turns} total turns so far; (max is #{max})\n"
			turns += 1
			@curr_player = (turns % 2 == 0) ? :white : :black
			puts "It's #{@curr_player.capitalize}'s turn... "
			@players[@curr_player].do_turn(@board)
		end

    if turns >= max && !@board.winner?
    	puts "GAME OVER: It's a draw!!"
    else
    	puts "GAME OVER: #{@curr_player.capitalize} is the winner!"
    end

  end	

end