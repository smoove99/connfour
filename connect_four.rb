#!/usr/bin/ruby

require_relative "lib/game.rb"

if $PROGRAM_NAME == __FILE__
  puts "\n\n---------------------\n"
  puts "Welcome to Connect Four!"
  puts "Select an option: "
  puts "1) One player vs. CPU"
  puts "2) Two players"
  puts

  while(true)
    input = gets.chomp
    if input !~ /[12]/
      puts "Invalid input #{input}! Try again" 
    else
      answer = input.to_i
      break
    end
  end

  case answer
  when 1
    player = PersonPlayer.new(:W)
    game = Game.new(player)
    game.play_game
  when 2
    player1 = PersonPlayer.new(:W)
    player2 = PersonPlayer.new(:B)
    game = Game.new(player1, player2)
    game.play_game
  # else
  #   puts "An error occurred; ending program"
  end

end


